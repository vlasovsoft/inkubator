#include <EEPROM.h>
#include <OneWire.h> 
#include <DallasTemperature.h>

// PINS
#define PIN_TEMP 2          // Atmega 328 pin 4
#define PIN_PWM 3           // Atmega 328 pin 5
#define PIN_LED_DATA 8      // Atmega 328 pin 14
#define PIN_LED_CLOCK 9     // Atmega 328 pin 15
#define PIN_LED_STROBE 10   // Atmega 328 pin 16
#define PIN_LED 13          // Atmega 328 pin 19

OneWire oneWire(PIN_TEMP); 
DallasTemperature sensors(&oneWire);

// ============ LED % KEYS
void sendCommand(uint8_t value)
{
  digitalWrite(PIN_LED_STROBE, LOW);
  shiftOut(PIN_LED_DATA, PIN_LED_CLOCK, LSBFIRST, value);
  digitalWrite(PIN_LED_STROBE, HIGH);
}
 
void reset()
{
  sendCommand(0x40); // set auto increment mode
  digitalWrite(PIN_LED_STROBE, LOW);
  shiftOut(PIN_LED_DATA, PIN_LED_CLOCK, LSBFIRST, 0xc0);   // set starting address to 0
  for(uint8_t i = 0; i < 16; i++)
  {
    shiftOut(PIN_LED_DATA, PIN_LED_CLOCK, LSBFIRST, 0x00);
  }
  digitalWrite(PIN_LED_STROBE, HIGH);
}

void writeSymbol(int pos, byte symbol)
{
  digitalWrite(PIN_LED_STROBE, LOW);
  shiftOut(PIN_LED_DATA, PIN_LED_CLOCK, LSBFIRST, 0xc0+2*pos);
  shiftOut(PIN_LED_DATA, PIN_LED_CLOCK, LSBFIRST, symbol);
  digitalWrite(PIN_LED_STROBE, HIGH);  
}

void clearScreen()
{
  for (int i=0; i<8; i++) {
    writeSymbol(i, 0);
  }
}

void writeDigit(int pos, byte digit, bool dot)
{
  byte sym = 0b00000000;
  static byte digits[] =   
    { 0b00111111, // 0
      0b00000110, // 1
      0b01011011, // 2
      0b01001111, // 3
      0b01100110, // 4
      0b01101101, // 5
      0b01111101, // 6
      0b00000111, // 7
      0b01111111, // 8
      0b01101111  // 9
    };
  if ( digit < 10 ) {
      sym = digits[digit];
      if ( dot ) sym |= 0b10000000;
  }
  digitalWrite(PIN_LED_STROBE, LOW);
  shiftOut(PIN_LED_DATA, PIN_LED_CLOCK, LSBFIRST, 0xc0+2*pos);
  shiftOut(PIN_LED_DATA, PIN_LED_CLOCK, LSBFIRST, sym);
  digitalWrite(PIN_LED_STROBE, HIGH);  
}

// return 8 bit
uint8_t readButtons()
{
  uint8_t buttons = 0;
  digitalWrite(PIN_LED_STROBE, LOW);
  shiftOut(PIN_LED_DATA, PIN_LED_CLOCK, LSBFIRST, 0x42);
  pinMode(PIN_LED_DATA, INPUT);
  
  uint8_t v = 0;
  
  v = shiftIn(PIN_LED_DATA, PIN_LED_CLOCK, LSBFIRST);
  if ( v & 0b00000001 )
      buttons |= 0b10000000;
  if ( v & 0b00010000 )
      buttons |= 0b00001000;
  
  v = shiftIn(PIN_LED_DATA, PIN_LED_CLOCK, LSBFIRST);
  if ( v & 0b00000001 )
      buttons |= 0b01000000;
  if ( v & 0b00010000 )
      buttons |= 0b00000100;

  v = shiftIn(PIN_LED_DATA, PIN_LED_CLOCK, LSBFIRST);
  if ( v & 0b00000001 )
      buttons |= 0b00100000;
  if ( v & 0b00010000 )
      buttons |= 0b00000010;

  v = shiftIn(PIN_LED_DATA, PIN_LED_CLOCK, LSBFIRST);
  if ( v & 0b00000001 )
      buttons |= 0b00010000;
  if ( v & 0b00010000 )
      buttons |= 0b00000001;

  pinMode(PIN_LED_DATA, OUTPUT);
  digitalWrite(PIN_LED_STROBE, HIGH);

  return buttons;
}

// value is 1="on", 0="0ff"
// position is 0 (left) ... 7 (right)
void setLed(uint8_t value, uint8_t position)
{
  pinMode(PIN_LED_DATA, OUTPUT); 
  sendCommand(0x44);
  digitalWrite(PIN_LED_STROBE, LOW);
  shiftOut(PIN_LED_DATA, PIN_LED_CLOCK, LSBFIRST, 0xC1 + (position << 1));
  shiftOut(PIN_LED_DATA, PIN_LED_CLOCK, LSBFIRST, value);
  digitalWrite(PIN_LED_STROBE, HIGH);
}

void setLeds(uint8_t value)
{
  for ( int i=0; i<8; ++i )
  {
    uint8_t v = ((value << i) & 0b10000000) != 0 ? 1:0;
    setLed(v, i);    
  }
}

// ============ LED % KEYS end

void setTemperature( double val )
{
  analogWrite(PIN_PWM, round(val));
}

void writeTemperature(int pos, double t)
{
  if ( t > 0 && t < 99 ) {
    int temp = round(t*100.0);
    writeDigit(pos+0, temp/1000, false);
    writeDigit(pos+1, (temp%1000)/100, true);
    writeDigit(pos+2, (temp%100)/10, false);
    writeDigit(pos+3, temp%10, false);    
  } else {
    writeSymbol(pos+0, 0b01000000);
    writeSymbol(pos+1, 0b01000000);
    writeSymbol(pos+2, 0b01000000);
    writeSymbol(pos+3, 0b01000000);    
  }  
}

void writeCorrection(int pos, double c)
{
  int temp = round(fabs(c)*100.0);
  writeSymbol(pos+0, c >= 0 ? 0b00000000:0b01000000);
  writeDigit(pos+1, temp/100, true);
  writeDigit(pos+2, (temp%100)/10, false);  
  writeDigit(pos+3, temp%10, false);
}

void writeVal1(int pos, int val)
{
  writeDigit(pos+0, val, false);
}

void writeVal2(int pos, int val)
{
  writeDigit(pos+0, val/10, false);
  writeDigit(pos+1, val%10, false);
}

void writeVal3(int pos, int val)
{
  writeDigit(pos+0, val/100, false);
  writeDigit(pos+1, (val%100)/10, false);
  writeDigit(pos+2, val%10, false);
}

void setup() {
  TCCR1A = TCCR1A & 0xe0 | 1;
  TCCR1B = TCCR1B & 0xe0 | 0x0b;
  pinMode(PIN_PWM, OUTPUT);
  pinMode(PIN_LED_STROBE, OUTPUT);
  pinMode(PIN_LED_CLOCK, OUTPUT);
  pinMode(PIN_LED_DATA, OUTPUT);
  sensors.begin();
  sensors.setWaitForConversion(true);
  sensors.setResolution(12);
  sendCommand(0b10001001);
  reset();
  eeRead();
}

double temp = 20.0;
double t_err = 0.05;
double t_eta = 38.0;
double correction = 0.0;

int pwmMin = 0;
int pwmMax = 255;
int pwmAvg = (pwmMin + pwmMax)/2;
int pwm = pwmMin;

int counter = 1;

void eeWrite()
{
  int address = 0;
  EEPROM.put(address, (byte)0); address += sizeof(byte);
  EEPROM.put(address, t_eta); address += sizeof(float);
  EEPROM.put(address, correction); address += sizeof(float);
}

void eeRead()
{
  int address = 0;
  byte val = 0xFF;
  EEPROM.get(address, val); address += sizeof(byte);
  if ( 0 == val )
  {
      EEPROM.get(address, t_eta); address += sizeof(float);
      EEPROM.get(address, correction); address += sizeof(float);
  }
  else
  {
      t_eta = 38.0;
      correction = 0.0;
  }
}

enum Mode {
  modeNorm,
  modeOff,
  modeTemp,
  modeCorr 
} mode = modeNorm;

void processModeNorm(uint8_t buttons)
{
  writeTemperature(4, temp); 

  counter--;

  if ( counter <= 0 )
  {  
    counter = 4;

    digitalWrite(PIN_LED, HIGH);
    sensors.requestTemperatures();
    digitalWrite(PIN_LED, LOW);
    temp = sensors.getTempCByIndex(0);
    if (DEVICE_DISCONNECTED_C == temp) {
      temp = 0;      
    } else {
      temp += correction;      
    }
    
    writeTemperature(4, temp);

    if ( 0 == temp ) {
        pwm = pwmMin;
        writeSymbol(0, 0b1000000);
    }
    else 
    if ( temp >= t_eta+t_err )
    {
        pwm = pwmMin;
        writeSymbol(0, 0b0001000);
    }
    else
    if ( temp <= t_eta-t_err )
    {
        pwm = pwmMax;
        writeSymbol(0, 0b0000001);
    }
    else
    {
        pwm = pwmAvg;
        writeSymbol(0, 0b1000000);
    }
        
    setTemperature( pwm );
  }

  delay(300);
}

void processModeOff(uint8_t buttons)
{
  // print "Off"
  writeSymbol(0, 0b00111111);
  writeSymbol(1, 0b01110001);
  writeSymbol(2, 0b01110001);

  writeTemperature(4, temp); 

  counter --;

  if ( counter <= 0 )
  {
    counter = 4;
          
    digitalWrite(PIN_LED, HIGH);
    sensors.requestTemperatures();
    digitalWrite(PIN_LED, LOW);
    temp = sensors.getTempCByIndex(0);
    if (DEVICE_DISCONNECTED_C == temp) {
      temp = 0;      
    } else {
      temp += correction;      
    }
    writeTemperature(4, temp);
  }

  delay(300);
}

void processModeTemp(uint8_t buttons)
{
  // print "t"
  writeSymbol(0, 0b01111000);
  switch( buttons )
  {
  case 0b00001000:
      t_eta = constrain(t_eta-0.1, 37, 39);
      break;
  case 0b00000100:
      t_eta = constrain(t_eta-0.01, 37, 39);
      break;    
  case 0b00000010:
      t_eta = constrain(t_eta+0.01, 37, 39);
      break;
  case 0b00000001:
      t_eta = constrain(t_eta+0.1, 37, 39);
      break;
  }

  writeTemperature(4, t_eta);

  delay(300);
}

void processModeCorr(uint8_t buttons)
{
    // print "C"
  writeSymbol(0, 0b00111001);
  switch( buttons )
  {
  case 0b00001000:
      correction = constrain(correction-0.1, -9.9, 9.9);
      break;
  case 0b00000100:
      correction = constrain(correction-0.01, -9.9, 9.9);
      break;
  case 0b00000010:
      correction = constrain(correction+0.01, -9.9, 9.9);
      break;
  case 0b00000001:
      correction = constrain(correction+0.1, -9.9, 9.9);
      break;
  }

  writeCorrection(4, correction);

  delay(300);
}

void loop() 
{     
  Mode oldMode = mode;
  
  // process buttons
  uint8_t buttons = readButtons();
  switch( buttons )
  {
  case 0b10000000:
      mode = modeNorm;      
      break;
  case 0b01000000:
      mode = modeOff;
      break;
  case 0b00100000:
      mode = modeTemp;
      break;
  case 0b00010000:
      mode = modeCorr;
      break;
  }

  if ( mode != oldMode )
  {
    if ( modeTemp == oldMode || modeCorr == oldMode )
    {
      eeWrite();
    }
    if ( modeNorm == mode || modeTemp == mode ) 
    {
      counter = 1;
    }
    if ( modeTemp == mode || modeCorr == mode || modeOff == mode )
    {
      pwm = pwmMin;
      setTemperature(pwm);
    }
    clearScreen();
  }
 
  switch( mode )
  {
  case modeNorm:
      processModeNorm(buttons);
      break;
  case modeOff:
      processModeOff(buttons);
      break;
  case modeTemp:
      processModeTemp(buttons);
      break;
  case modeCorr:
      processModeCorr(buttons);
      break;
  default:
      break;   
  }
}
